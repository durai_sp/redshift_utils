"""
handler.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 11, 2017

handle functionality is used to initiate prod trip event.
"""

from dashboard import Dashboard
import sys
import collections
import json

def handler(stream_name=sys.argv[1], table_name=sys.argv[2], region_name=sys.argv[3], level=sys.argv[4], table_meta_data=sys.argv[5], sort_key=sys.argv[6], index_key=sys.argv[7]):
    data=json.loads(table_meta_data)
    order_data = collections.OrderedDict(sorted(data.items()))
    dashboard = Dashboard(stream_name, table_name, region_name, level)  
    dashboard.handler(order_data, sort_key, index_key)

if __name__ == "__main__":
    handler()
