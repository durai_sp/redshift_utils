"""
Dashboard.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 12, 2017

Dashboard class has the functionality to load data to S3
"""

from load_data_s3 import LoadDataS3
from platformutils.utils.time_util import TimeUtil

class Dashboard(object):

    def __init__(self, stream_name, table_name, r_name, level):
	self.stream_name = stream_name
	self.table_name = table_name
	self.region = r_name
	self.level = level
	self.time =TimeUtil()    


    def handler(self, table_meta_data, key, index_name=None):
	load = LoadDataS3(self.stream_name, self.table_name, self.region, self.level)
	utc_value = self.time.get_utc_timestamp(1, 'ms')
	load.get_data(table_meta_data, key, utc_value, index_name)

